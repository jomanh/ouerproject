<?php

use App\Http\Controllers\GeneralController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\BlogCatagoryController;
use App\Http\Controllers\MediaController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Route::get('/', function () {
//    return view('front.welcome');
//});


Route::get('/', [GeneralController::class, 'index']);
Route::get('/blog/{blog}', [GeneralController::class, 'blogDetails'])->name('blog.details');
Route::post('/blog-search', [GeneralController::class, 'blogSearch'])->name('blog.search');

Route::resource('/blogs', BlogController::class);
Route::resource('/catagory', BlogCatagoryController::class);

//media
Route::post('projects/media/{table}', [MediaController::class, 'storeMedia'])
    ->name('projects.storeMedia');
Route::delete('projects/media/{table}', [MediaController::class, 'destroyMedia'])
    ->name('projects.deleteMedia');
