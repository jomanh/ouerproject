<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    //

    public function index()
    {
        $cats = Category::with('blogs')->get();
        return view('front.index', compact('cats'));
    }

    public function blogDetails(Blog $blog)
    {

        $blog = Blog::with('cat')->find($blog->id);
        return view('front.blog', compact('blog'));
    }

    public function blogSearch(Request $request )
    {

        $blogs = Blog::with('cat')->where('title', 'like', '%' .$request->search . '%') ->orWhere('description', 'like', '%' .$request->search . '%')->get() ;

        return view('front.search', compact('blogs'));
    }

}
